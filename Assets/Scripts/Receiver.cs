﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System;
using UnityParseHelpers;

[RequireComponent(typeof(Loom))]
public class Receiver : MonoBehaviour
{
    public Material renderMat;
	public RawImage image;
	public bool enableLog = false;

    public int bufferSize;

	const int port = 1337;
	public string IP = "192.168.1.165";

	//My stuff
	private Loom Loom;
	TcpListener listener;

	TcpClient client;

	Texture2D tex;

	private bool stop = false;

	//This must be the-same with SEND_COUNT on the server
	const int SEND_RECEIVE_COUNT = 15;

	// Use this for initialization
	void Start()
	{
		listener = new TcpListener (new IPEndPoint (IPAddress.Loopback, port));
		Loom = GetComponent<Loom> ();
		Application.runInBackground = true;

		tex = new Texture2D(0, 0);
		client = new TcpClient();

		//Connect to server from another Thread
		Loom.RunAsync(() =>
			{
				LOGWARNING("Connecting to server...");
				// if on desktop
				//client.Connect(IPAddress.Loopback, port); //REMOVED BY ME
				listener.Start();
				client = listener.AcceptTcpClient();
                Debug.Log("Client accepted");

				// if using the IPAD
				//client.Connect(IPAddress.Parse(IP), port);
				LOGWARNING("Connected!");

				imageReceiver();
			});
	}


	void imageReceiver()
	{
		//While loop in another Thread is fine so we don't block main Unity Thread
		Loom.RunAsync(() =>
			{
                Debug.Log("receiving length");
				while (!stop)
				{
					//Read Image Count
					int imageSize = readImageByteSize(15); //COMMENTED OUT B Y ME
                    //int imageSize = readImageByteSize(SEND_RECEIVE_COUNT); //COMMENTED OUT B Y ME
                    Debug.Log(imageSize);
                    LOGWARNING("Received Image byte Length: " + imageSize);

					//Read Image Bytes and Display it
					readFrameByteArray(imageSize); // replaced "imageSize" with custom value 506531
				}
			});
	}


	//Converts the data size to byte array and put result to the fullBytes array
	void byteLengthToFrameByteArray(int byteLength, byte[] fullBytes)
	{
		//Clear old data
		Array.Clear(fullBytes, 0, fullBytes.Length);
		//Convert int to bytes
		byte[] bytesToSendCount = BitConverter.GetBytes(byteLength);
		//Copy result to fullBytes
		bytesToSendCount.CopyTo(fullBytes, 0);
	}

	//Converts the byte array to the data size and returns the result
	int frameByteArrayToByteLength(byte[] frameBytesLength)
	{
		int byteLength = BitConverter.ToInt32(frameBytesLength, 0);
		return byteLength;
	}


	/////////////////////////////////////////////////////Read Image SIZE from Server///////////////////////////////////////////////////
	private int readImageByteSize(int size)
	{
		bool disconnected = false;

		NetworkStream serverStream = client.GetStream();
		byte[] imageBytesCount = new byte[size];
		var total = 0;
		do
		{
			var read = serverStream.Read(imageBytesCount, total, size - total);
			//Debug.LogFormat("Client recieved {0} bytes", total);
			if (read == 0)
			{
				disconnected = true;
				break;
			}
			total += read;
		} while (total != size);

		int byteLength;

		if (disconnected)
		{
			byteLength = -1;
		}
		else
		{
			byteLength = frameByteArrayToByteLength(imageBytesCount);
		}
		return byteLength;
	}

	/////////////////////////////////////////////////////Read Image Data Byte Array from Server///////////////////////////////////////////////////
	private void readFrameByteArray(int size)
	{
		bool disconnected = false;

		NetworkStream serverStream = client.GetStream();
        //byte[] lengthBuffer = new byte[size];
        //int length;
		byte[] imageBytes = new byte[size]; // replaced "size" with custom value = 506531
		var total = 0;
        //length = serverStream.Read(lengthBuffer, 0, 4);
        do
		{
			var read = serverStream.Read(imageBytes, total, size - total);
            Debug.Log("Reading");
			//Debug.LogFormat("Client recieved {0} bytes", total);
			if (read == 0)
			{
				disconnected = true;
				break;
			}
			total += read;
		} while (total < size); // changed to less than from !=

		bool readyToReadAgain = false;

		//Display Image
		if (!disconnected)
		{
			//Display Image on the main Thread
			Loom.QueueOnMainThread(() =>
				{
                    Debug.Log("display new");
					displayReceivedImage(imageBytes);
					readyToReadAgain = true;
				});
		}

		//Wait until old Image is displayed
		while (!readyToReadAgain)
		{
			System.Threading.Thread.Sleep(1);
		}
	}


	void displayReceivedImage(byte[] receivedImageBytes)
	{
		Debug.Log ("Displaying new received image!");
		tex.LoadImage(receivedImageBytes);
        //image.texture = tex;
        renderMat.SetTexture("_MainTex", tex);
	}


	// Update is called once per frame
	void Update()
	{


	}


	void LOG(string messsage)
	{
		if (enableLog)
			Debug.Log(messsage);
	}

	void LOGWARNING(string messsage)
	{
		if (enableLog)
			Debug.LogWarning(messsage);
	}

	void OnApplicationQuit()
	{
		LOGWARNING("OnApplicationQuit");
		stop = true;

		if (client != null)
		{
			client.Close();
		}
	}
}