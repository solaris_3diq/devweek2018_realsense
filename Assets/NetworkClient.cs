﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;


public class NetworkClient : MonoBehaviour {

	public Texture2D proxyTextureRef;
	public static Texture2D texture;

	TcpListener tcpListener;
	TcpClient client;

	Thread readThread;

	private DataReader dataReader;

	public static NetworkStream networkStream;

	// Use this for initialization
	void Start () {
		texture = proxyTextureRef;
		tcpListener = new TcpListener (new IPEndPoint(IPAddress.Loopback, 1337));
		tcpListener.Start ();
		client = tcpListener.AcceptTcpClient ();
		networkStream = client.GetStream ();
		dataReader = new DataReader ();
		readThread = new Thread (new ThreadStart (dataReader.StartReading));
		readThread.Start ();
		//StartCoroutine (ReadData ());
	}

	IEnumerator ReadData() {
		
		while (true) {
			byte[] incomingBytes = new byte[506531];
			networkStream.Read (incomingBytes, 0, 506531);
			texture.LoadImage (incomingBytes);
			yield return null;
		}
	
	}

	public static void LoadImage(byte[] dataToLoad) {
		texture.LoadImage (dataToLoad);
	}
}

public class DataReader {

	private Texture2D texture;
	private NetworkStream networkStream;

	public DataReader() {
	}

	public DataReader(Texture2D texture, NetworkStream networkStream) {
		this.texture = texture;
		this.networkStream = networkStream;
		StartReading ();
	}

	public void StartReading() {
		while (true) {
			byte[] incomingBytes = new byte[506531];
			//NetworkClient.networkStream.Read (incomingBytes, 0, 506531);
			//NetworkClient.networkStream.BeginRead(incomingBytes, 0, 506531, 
			NetworkClient.LoadImage (incomingBytes);
			//texture.LoadImage (incomingBytes);
		}

	}

}
	
