﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using System.Linq;

[RequireComponent(typeof(CustomRenderTexture))]
public class RenderTextureController : MonoBehaviour {

	public Texture2D testTexture;
	public Texture2D secondTextTexture;
	public Texture2D[] testTextures;
	private int index = 0;
	public CustomRenderTexture renderTexture;
	public Material mainMat;

	private Renderer renderer;

	private int width = 1024;
	private int height =  1024;

	private Color32 color = new Color32((byte)255, (byte)255, (byte)255, (byte)255);

	// Use this for initialization
	void Start () {
		renderer = GetComponent<MeshRenderer> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//renderTexture.material.SetTexture ();
		index++;
		if (index >= testTextures.Length)
			index = 0;
		//Color[] newPixels = testTextures [index].GetPixels ();
		Color32[] colors = Enumerable.Repeat(color, width*height).ToArray();

		testTexture.SetPixels32 (colors);

		//mainMat.SetTexture("_MainTex", testTextures[index]);
		//renderer.material = mainMat;
	}
}
